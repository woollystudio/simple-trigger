# Simple-trigger

Mobile app template made with the Android mode in Processing to trigger simple events as sounds, flashlight blinks, etc. Including an example made for "Jacominus". Here is a screenshot from a random android device :

<div align="center">
  <img width="200" src="Jacominus-example.jpeg">
</div>
