import android.os.Bundle;
import android.view.WindowManager;    

import cassette.audiofiles.SoundFile;

import ketai.camera.*;

// --

// Head title and credits
final String project_title = "Jacominus";
final String app_name = "sound trigger";
final String app_credits = "Sound trigger prototype (cc) woollystud.io";

// Set font size function of screensize
final int text_size = int(24 * displayDensity);
//final int text_size = int(32 * displayDensity);

// Matrix-size you need
final float x_matrix = 3;
final float y_matrix = 4;

// Top offset for headline
final float top_offset = 100;

// Number of the flashlight-attached trigger,
final int flash_trigger = 0;

// Bottom offset for credits
final float bottom_offset = 50;

int trigger_number = int(x_matrix * y_matrix);

final String [] label = new String [trigger_number + 1];

// --

SoundFile [] audio = new SoundFile [trigger_number + 1];

KetaiCamera cam;

PFont menlo;

void setup()
{
  orientation(PORTRAIT);
  
  // Init a new camera,
  // to interact with flashlight
  // /!\ Do not forget to add camera in sketch permissions
  cam = new KetaiCamera(this, 320, 240, 24);
  cam.start();
  
  // Index labels
  label[0] = "Cliché";
  label[1] = "Sms";
  label[2] = "Sonnerie";
  
  // Empty triggers are filled with a null label
  // (here from the third, to the last trigger)
  for(int i = 3; i < label.length; i++) label[i] = "null";
  
  // Index audio files
  for(float i = 0; i < x_matrix; i++){
    for(float j = 0; j < y_matrix; j++){
      
      int index = int(i+j*x_matrix); // Index from left to right, top to bottom : 0 | 1 | 2
                                     //                                           3 | 4 | 5
                                     //                                           6 | 7 | 8  etc.
      
      // Glue triggers with soundfiles (audio_0 to audio_n) if their labels is not null,
      // if they are, glue the "empty" triggers with the empty wav file called "audio_null.wav"
      // (to add soundfiles : create a data folder if doesn't exists in the sketch patch and put them in)
      if(label[index] != "null") audio[index] = new SoundFile(this, "audio_" + index + ".wav");
      else audio[index] = new SoundFile(this, "audio_null.wav");
    }
  }
  
  // Init fonts
  menlo = createFont("menlo.ttf", text_size);
  textAlign(CENTER, CENTER);
  textFont(menlo);
  
  // End of setup
  background(0);
  draw_head_title(#f93143);
  draw_credits();
}

void draw()
{
  background(0);
  if (cam != null && cam.isStarted()) draw_head_title(255);
  else draw_head_title(#f93143);
  draw_credits();
  
  build_matrix();
}

void build_matrix()
{
  for(float i = 0; i < x_matrix; i++){
    for(float j = 0; j < y_matrix; j++){
      
      int index = int(i+j*x_matrix); // Index from left to right, top to bottom : 0 | 1 | 2
                                     //                                           3 | 4 | 5
                                     //                                           6 | 7 | 8  etc.
      
      if(mousePressed == true &&
         mouseX >= i * (width-1) / x_matrix && mouseX <= (i+1) * (width-1) / x_matrix && 
         mouseY >= top_offset+j * (height-top_offset-bottom_offset-1) / y_matrix && mouseY <= top_offset + (j+1) * (height-top_offset-bottom_offset-1) / y_matrix) {
           
         fill(#f93143); // Fill red
         
         // Enable flash
         if(index == flash_trigger){
           if(cam.isFlashEnabled()) cam.disableFlash();
           cam.enableFlash();
         }
         
         // Play audio file
         audio[index].play();
         
         // Disable flash
         if(index == flash_trigger && cam.isFlashEnabled()){
           delay(100);
           cam.disableFlash();
         }
         
      }  else {
        
         fill(#f1f1f1);
      }
      float x = i*(width-1)/x_matrix;
      float y = top_offset+j*(height-top_offset-bottom_offset-1)/y_matrix;
      
      rect(x, y, width/x_matrix, (height-top_offset-bottom_offset-1)/y_matrix);
      
      fill(0);
      textSize(text_size);
      
      if(label[index] != "null") text(label[index], x + width/x_matrix/2, y + (height-top_offset-bottom_offset-1)/y_matrix/2);
    }
  }
}

void draw_head_title(color c)
{
  fill(c);
  textSize(text_size);
  
  text(project_title + "_app, " + app_name, width/2, top_offset/2);
}

void draw_credits()
{
  fill(200);
  textSize(text_size*0.66);
  
  text(app_credits, width/2, height-bottom_offset/2);
}

// Force screen to be always ON
void onCreate(Bundle bundle) {
  super.onCreate(bundle);
  getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
}

// Quit when on pause
public void onPause() { 
  System.exit(0);
  super.onPause();
}
